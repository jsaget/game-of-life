from main import *
from typing import Set, Tuple
from time import sleep


def init() -> Grid:
    height: int = int(input("Hauteur de la grille : "))
    width: int = int(input("Largeur de la grille : "))
    data: Set[Tuple[int, int]] = set()
    grid = Grid(height, width, data)
    print("== État de la grille ==")
    print(grid)
    another_input = input("Changer une autre case ? [Y/n]")
    another: bool = another_input.upper() != "N"
    while another:
        x = int(input("Ligne de la case à changer : "))
        y = int(input("Colonne de la case à changer : "))
        if not grid._in_bound(x, y):
            print("Erreur: coordonnées hors bornes")
            another = True
        else:
            grid.content[x, y] = not (grid.content[x, y])
            print("== État de la grille ==")
            print(grid)
            another_input = input("Changer une autre case ? [Y/n]")
            another = another_input.upper() != "N"
    return grid


def simulate(grid: Grid, n: int, wait: float = 0.0) -> None:
    print("Début de la simulation de {} étape(s) du jeu de la vie".format(n))
    k: int
    print("Étape 0")
    print(grid)
    for k in range(1, n + 1):
        if wait < 0.0:
            input("(Appuyer sur <Entrée> pour continuer)")
        else:
            sleep(wait)
        grid.next()
        print("Étape {}".format(k))
        print(grid)


def main() -> None:
    grid = init()
    stop: bool = False
    while not stop:
        simulate(grid, 30, 0.5)
        stop_input = input("Continuer ? [y/N]")
        stop = stop_input.upper() != "Y"


main()
