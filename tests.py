import pytest
from main import *

## Tests for counting neighbours


def test_init_grid():
    my_grid = Grid(7, 7, {(2, 3)})
    assert my_grid.count_neighbours(2, 3) == 0


def test_init2():
    with pytest.raises(AssertionError):
        my_grid = Grid(-1, -1, set())


def test_init3():
    with pytest.raises(AssertionError):
        my_grid = Grid(12, 12, {(42, 5)})


def test_count_neighbours():
    my_grid = Grid(12, 12, {(4, 8), (4, 7)})
    assert my_grid.count_neighbours(4, 8) == 1


def test_count_neighbours2():
    my_grid = Grid(20, 20, {(4, 8), (4, 7), (5, 8), (5, 9), (3, 7)})
    assert my_grid.count_neighbours(4, 8) == 4


def test_count_neighbours_boundaries():
    my_grid = Grid(5, 5, {(4, 4)})
    assert my_grid.count_neighbours(0, 0) == 0


## Tests for next state


def test_next_state_cell():
    my_grid = Grid(12, 12, {(4, 8), (5, 7), (6, 8)})
    assert my_grid.next_state_cell(5, 8)


def test_next_state_cell2():
    my_grid = Grid(12, 12, set())
    assert not my_grid.next_state_cell(5, 8)


def test_next_state_cell_dies():
    my_grid = Grid(12, 12, {(x, y) for x in range(6, 9) for y in range(8, 11)})
    assert not my_grid.next_state_cell(7, 9)


def test_next_state_cell_dies2():
    my_grid = Grid(12, 12, {(1, 1), (1, 2), (1, 3), (2, 2), (2, 3)})
    assert not my_grid.next_state_cell(1, 2)


def test_next_state_cell_stay_dead():
    my_grid = Grid(12, 12, {(2, 2), (3, 3)})
    assert not my_grid.next_state_cell(2, 3)


def test_next_state_cell_stay_dead2():
    my_grid = Grid(2, 2, {(1, 1)})
    assert not my_grid.next_state_cell(0, 1)


def test_next_state_cell_stay_alive():
    my_grid = Grid(3, 3, {(0, 0), (0, 1), (0, 2)})
    assert my_grid.next_state_cell(0, 1)


def test_next_state_out_of_bound():
    my_grid = Grid(12, 12, set())
    with pytest.raises(AssertionError):
        my_grid.next_state_cell(2, 14)
    with pytest.raises(AssertionError):
        my_grid.next_state_cell(-1, 0)
    with pytest.raises(AssertionError):
        my_grid.next_state_cell(18, 2)
    my_grid2 = Grid(19, 15, set())
    assert not my_grid2.next_state_cell(18, 14)


## Test equality


def test_eq():
    assert Grid(10, 10, set()) == Grid(10, 10, set())


def test_neq():
    assert Grid(10, 10, set()) != Grid(42, 42, set())


def test_neq2():
    assert not (Grid(10, 10, set()) == Grid(10, 10, {(2, 3)}))


## Test for updating the whole grid


def test_next_grid_void():
    my_grid = Grid(10, 10, set())
    my_grid.next()
    assert my_grid == Grid(10, 10, set())


def test_next_grid_dies():
    my_grid = Grid(10, 10, {(1, 1)})
    my_grid.next()
    assert my_grid == Grid(10, 10, set())


def test_next_grid_survivor():
    my_grid = Grid(10, 10, {(1, 1), (0, 0), (2, 2)})
    my_grid.next()
    assert my_grid == Grid(10, 10, {(1, 1)})


# Test Affichage


def test_print():
    my_grid = Grid(4, 4, set())
    res = "....\n....\n....\n...."
    assert str(my_grid) == res


def test_print2():
    my_grid = Grid(3, 3, set())
    res = "...\n...\n..."
    assert str(my_grid) == res


def test_print3():
    my_grid = Grid(3, 3, {(1, 1)})
    res = "...\n.*.\n..."
    assert str(my_grid) == res
