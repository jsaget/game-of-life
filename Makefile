.PHONY: all, type, test

all: type test run

type: main.py
	mypy --strict main.py

test: tests.py main.py
	pytest tests.py

run: demo.py type
	mypy --strict demo.py
	python3 demo.py
