from typing import Set, Tuple
import numpy as np
import numpy.typing as npt


class Grid:
    def __init__(self, height: int, width: int, coords: Set[Tuple[int, int]]):
        assert height >= 0 and width >= 0
        self.content = np.zeros((height, width), dtype=bool)
        self.height: int = height
        self.width: int = width
        for (x, y) in coords:
            assert self._in_bound(x, y)
            self.content[x, y] = True

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Grid):
            raise NotImplemented
        if self.height == other.height:
            if self.width == other.width:
                b: bool = (self.content == other.content).all()
                return b
        return False

    def _in_bound(self, x: int, y: int) -> bool:
        if x >= 0 and x < self.height:
            if y >= 0 and y < self.width:
                return True
        return False

    def count_neighbours(self, x: int, y: int) -> int:
        n: int = 0
        for dx in range(-1, 2):
            for dy in range(-1, 2):
                if (dx != 0 or dy != 0) and self._in_bound(x + dx, y + dy):
                    if self.content[x + dx, y + dy]:
                        n += 1
        return n

    def next_state_cell(self, x: int, y: int) -> bool:
        n: int = self.count_neighbours(x, y)
        assert self._in_bound(x, y)
        if n < 2 or n > 3:
            return False
        elif n == 2:
            b: bool = self.content[x, y]
            return b
        return True

    def next(self) -> None:
        new_content = np.zeros((self.height, self.width), dtype=bool)
        for x in range(self.height):
            for y in range(self.width):
                new_content[x, y] = self.next_state_cell(x, y)
        self.content = new_content

    def __str__(self) -> str:
        res = []
        for x in range(self.height):
            line: str = ""
            for y in range(self.width):
                if self.content[x, y]:
                    line += "*"
                else:
                    line += "."
            res.append(line)
        return "\n".join(res)
